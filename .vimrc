set mouse=""
set encoding=utf-8

set foldmethod=marker
set tabstop=2
set softtabstop=2
set expandtab
set shiftwidth=2
set autochdir
set t_Co=256
imap jj <Esc>
cmap jj <c-c>
vmap v <Esc>

map ; :
set number
set list listchars=tab:»·,trail:·

let gmapleader=','

nnoremap <leader><leader> <c-^>

set autowrite


set pastetoggle=<F2>
set clipboard=unnamed

nnoremap / /\v
vnoremap / /\v

set hlsearch
set incsearch
set ignorecase
set smartcase
map <CR :nohl<CR>


nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

set title

filetype plugin on 

set undofile
set undodir=/.config/vim/undodir

call plug#begin('~/.config/vim/plugged')
  Plug 'sheerun/vim-polyglot'
  Plug 'tpope/vim-surround'
  Plug 'mattn/emmet-vim'
  Plug 'jiangmiao/auto-pairs'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
  Plug 'junegunn/fzf.vim'
  Plug 'scrooloose/nerdtree'
  Plug 'altercation/vim-colors-solarized'
  Plug 'joshdick/onedark.vim'
  Plug 'itchyny/lightline.vim'
  Plug 'morhetz/gruvbox'
    map <leader>f :Files<CR>
    map <C-P> :GFiles<CR>
    map <C-B> :Buffer<CR>
  Plug 'luochen1990/rainbow'
    let g:rainbow_active = 1
  Plug 'aaren/arrowkeyrepurpose'
  Plug 'EinfachToll/DidYouMean'
call plug#end()

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste'  ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified'  ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

syntax enable
set background=dark
colorscheme gruvbox
set noshowmode
set laststatus=2
execute pathogen#infect()
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
